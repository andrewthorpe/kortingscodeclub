<?php

return [

    // 'question' => 'answere'
    'Hoe vaak brengt {{COMPANY_NAME}} nieuwe kortings codes uit?' => 'De laatste tijden ontdekken wij <strong>elke {{NEW_COUPON_EVERY_X_DAYS}} dagen</strong> een nieuwe kortings code bij {{COMPANY_NAME}}. De laatste {{LAST_X_DAYS}} dagen hebben we {{NEW_COUPONS_COUNT_IN_LAST_X_DAYS}} nieuwe coupons gevonden bij {{COMPANY_NAME}}.',
    'Hoeveel kan ik besparen door een coupon te gebruiken bij {{COMPANY_NAME}}?' => 'De meest populaire korting die we hebben kunnen vinden is {{MOST_POPULAIR_COUPON_TITLE}}. kortingscodeclub.nl shoppers besparen <strong>gemiddeld €{{USERS_SAVE_ON_AVERAGE_X}}</strong> bij betaling.',
    'Hoe ontvang ik de laatste deals van {{COMPANY_NAME}}?' => "Registreer je voor kortingscodeclub.nl's email alerts voor {{COMPANY_NAME}} en wij zullen je elke keer een email sturen zodra wij een nieuwe kortingscode hebben gevonden. Als je op dit moment geen werkende kortingscode kan vinden, registreer je dan en ontvang de nieuwste deals wanneer wij deze ontdekken.",
    'Wat is de beste manier om geld te besparen tijden het online winkel?' => 'Wij raden u aan om onze kortingscodeclub <a href="#">Chrome Extensie</a> te gebruiken zodat u geen deals meer mist tijden het shoppen.',
    'Kan ik een {{COMPANY_NAME}} kortings code toevoegen?' => 'Wij accepteren coupon code meldingen voor veel winkels. Bekijk onze <a href="route(\'submit\')">Nieuwe Coupon</a> pagina voor meer informatie over hoe je een nieuwe kortings code meld.'

];
