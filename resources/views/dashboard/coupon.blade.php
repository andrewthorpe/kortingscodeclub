@extends('dashboard.layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
          @if ($errors->any())
          <div class="alert alert-danger" role="alert">
            <ul>
              @foreach ($errors->all() as $error)
                  <li>{{$error}}</li>
              @endforeach
            </ul>
          </div>
          @endif

            <!-- Dropdown Card Example -->
            <div class="card shadow mb-4">
              <!-- Card Header - Dropdown -->
              <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <a href="{{ url()->previous() }}" class="btn btn-primary">< Back</a> <h6 class="m-0 font-weight-bold text-primary">Coupon</h6>
                <div class="dropdown no-arrow">
                  <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                    <div class="dropdown-header">Dropdown Header:</div>
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                  </div>
                </div>
              </div>
              <!-- Card Body -->
              <div class="card-body">



                {{-- FORM --}}

                <form method="POST" action="{{route('coupons.update', $coupon->id)}}">
                  @csrf
                  <input name="_method" type="hidden" value="PUT">

                  {{-- Name and Url --}}
                  <div class="form-group  row">
                    <div class="col-lg-3">
                      <label for="title" class="col-form-label">Title</label>
                      <input type="text" class="form-control" id="title" name="title" value="{{$coupon->title}}">
                    </div>
                    <div class="col-lg-9">
                      <label for="code" class="col-form-label">Code</label>
                      <input type="text" class="form-control" id="code" name="code" value="{{$coupon->code}}">
                    </div>
                  </div>

                  {{-- About --}}
                  <div class="form-group  row">
                    <div class="col-auto my-1">
                      <label class="mr-sm-2" for="status">Status</label>
                      <select name="status" class="custom-select mr-sm-2" id="status" >
                        <option value="10" @if($coupon->status == 10) selected @endif >Nieuw</option>
                        <option value="20" @if($coupon->status == 20) selected @endif>Geverifieerd</option>
                        <option value="30" @if($coupon->status == 30) selected @endif>Oud</option>
                        <option value="40" @if($coupon->status == 40) selected @endif>Verlopen</option>                    
                      </select>
                    </div>
                  </div>

                  {{-- used coupons count & success rate percentage --}}
                  <div class="form-group  row">
                    <div class="col-lg-4">
                      <label for="used" class="col-form-label">Used</label>
                      <input type="number" class="form-control" id="used" name="used" value="{{$coupon->used}}">
                    </div>
                    <div class="col-lg-4">
                      <label for="success_rate" class="col-form-label">Success rate, %</label>
                      <input type="number" class="form-control" id="success_rate" name="success_rate" value="{{$coupon->success_rate}}">
                    </div>
                  </div>


                  {{-- top_choice --}}
                  <div class="form-check">
                    <input type="hidden" value="0" name="top_choice">
                    <input onclick="$(this).val(this.checked ? 1 : 0)" class="form-check-input" type="checkbox" value="{{$coupon->top_choice}}" name="top_choice" id="top_choice" @if($coupon->top_choice) checked @endif >
                    <label class="form-check-label" for="top_choice">
                      Top choice
                    </label>
                  </div>

                  <hr>

                  <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
              
        </div>
    </div>
</div>

@endsection