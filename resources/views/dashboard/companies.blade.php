@extends('dashboard.layouts.app')

@section('content')

        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <h1 class="h3 mb-2 text-gray-800">Companies</h1>
            <p class="mb-4">Bekijk en edit hier de bedrijven.</p>
  
            <!-- DataTales Example -->
            <div class="card shadow mb-4">
              <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Companies</h6>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                      <tr>
                        <th>Edit</th>
                        <th>On top</th>
                        <th>Name</th>
                        <th>Url</th>
                        <th>About</th>
                        <th>Free returns</th>
                        <th>Free schipping</th>
                        <th>International schipping</th>
                        <th>Average saving</th>
                        <th>Available coupons</th>
                        <th>New coupons</th>
                        <th>Created at</th>
                        <th>Updated at</th>
                      </tr>
                    </thead>
                    <tfoot>
                      <tr>
                        <th>Edit</th>
                        <th>On top</th>
                        <th>Name</th>
                        <th>Url</th>
                        <th>About</th>
                        <th>Free returns</th>
                        <th>Free schipping</th>
                        <th>International schipping</th>
                        <th>Average saving</th>
                        <th>Available coupons</th>
                        <th>New coupons</th>
                        <th>Created at</th>
                        <th>Updated at</th>
                      </tr>
                    </tfoot>
                    <tbody>
                        @forelse ($companies as $company)
                        <tr>
                            <td><a class="btn btn-primary btn-round" href="{{route('companies.show', $company->id)}}">edit</a></td>
                            <td>
                                @if($company->top_choice)<i class="fas fa-check"></i>@else - @endif
                            </td>
                            <td>{{$company->name}}</td>
                            <td>{{$company->url}}</td>
                            <td>{{ Str::limit($company->about, 50) }}</td>
                            <td>@if($company->free_returns)<i class="fas fa-check"></i>@else - @endif</td>
                            <td>@if($company->free_shipping)<i class="fas fa-check"></i>@else - @endif</td>
                            <td>@if($company->international_shipping)<i class="fas fa-check"></i>@else - @endif</td>
                            <td>{{$company->average_saving}}</td>
                            <td>{{count($company->coupons)}}</td>
                            <td>{{count($company->coupons()->where('status', \App\Coupon::STATUS_NEW)->get())}}</td>
                            <td>{{$company->created_at}}</td>
                            <td>{{$company->updated_at}}</td>
                        </tr>
                        @empty
                            {{-- No companies --}}
                        @endforelse
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
  
          </div>
          <!-- /.container-fluid -->

      @endsection