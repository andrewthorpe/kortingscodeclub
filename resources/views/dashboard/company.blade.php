@extends('dashboard.layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
          @if ($errors->any())
          <div class="alert alert-danger" role="alert">
            <ul>
              @foreach ($errors->all() as $error)
                  <li>{{$error}}</li>
              @endforeach
            </ul>
          </div>
          @endif
          
            <!-- Dropdown Card Example -->
            <div class="card shadow mb-4">
              <!-- Card Header - Dropdown -->
              <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <a href="{{ route('companies.index') }}" class="btn btn-primary">< Back</a> <h6 class="m-0 font-weight-bold text-primary">{{ $company->name }}</h6>
                <div class="dropdown no-arrow">
                  <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                    <div class="dropdown-header">Dropdown Header:</div>
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                  </div>
                </div>
              </div>
              <!-- Card Body -->
              <div class="card-body">

                <form method="POST" action="{{route('companies.update', $company->id)}}">
                  @csrf
                  <input name="_method" type="hidden" value="PUT">

                  {{-- Name and Url --}}
                  <div class="form-group  row">
                    <div class="col-lg-3">
                      <label for="name" class="col-form-label">Name</label>
                      <input type="text" class="form-control" id="name" name="name" value="{{$company->name}}">
                    </div>
                    <div class="col-lg-9">
                      <label for="url" class="col-form-label">Url</label>
                      <input type="text" class="form-control" id="url" name="url" value="{{$company->url}}">
                    </div>
                  </div>

                  {{-- About --}}
                  <div class="form-group  row">
                    <div class="col-lg-12">
                      <label for="about" class="col-form-label">About</label>
                      <textarea type="text" class="form-control" id="about" name="about">{{$company->about}}</textarea>
                    </div>
                  </div>

                  {{-- Average saving price --}}
                  <div class="form-group  row">
                    <div class="col-lg-4">
                      <label for="average_saving" class="col-form-label">Average saving, €</label>
                      <input type="number" placeholder="1.0" step="0.01" min="0" name="average_saving" id="average_saving" value="{{$company->average_saving}}">
                    </div>
                  </div>

                  {{-- Company facts: top_choice, free_returns, free_shipping, international_shipping --}}
                    <div class="form-check">
                      <input type="hidden" value="0" name="top_choice">
                      <input onclick="$(this).val(this.checked ? 1 : 0)" class="form-check-input" type="checkbox" value="{{$company->top_choice}}" name="top_choice" id="top_choice" @if($company->top_choice) checked @endif >
                      <label class="form-check-label" for="top_choice">
                        Top choice
                      </label>
                    </div>
                    <div class="form-check">
                      <input type="hidden" value="0" name="free_returns">
                      <input onclick="$(this).val(this.checked ? 1 : 0)" class="form-check-input" type="checkbox" value="{{$company->free_returns}}" name="free_returns" id="free_returns" @if($company->free_returns) checked @endif >
                      <label class="form-check-label" for="free_returns">
                        Free returns
                      </label>
                    </div>
                    <div class="form-check">
                      <input type="hidden" value="0" name="free_shipping">
                      <input onclick="$(this).val(this.checked ? 1 : 0)" class="form-check-input" type="checkbox" value="{{$company->free_shipping}}" name="free_shipping" id="free_shipping" @if($company->free_shipping) checked @endif>
                      <label class="form-check-label" for="free_shipping">
                        Free shipping
                      </label>
                    </div>
                    <div class="form-check">
                      <input type="hidden" value="0" name="international_shipping">
                      <input onclick="$(this).val(this.checked ? 1 : 0)" class="form-check-input" type="checkbox" value="{{$company->international_shipping}}" name="international_shipping" id="international_shipping" @if($company->international_shipping) checked @endif>
                      <label class="form-check-label" for="international_shipping">
                        International shipping
                      </label>
                    </div>
                    <hr>
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>

                <hr>

                <h1>Coupons</h1>
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th>Edit</th>
                          <th>On top</th>
                          <th>Title</th>
                          <th>Code</th>
                          <th>Status</th>
                          <th>Used</th>
                          <th>Last used</th>
                          <th>Success rate</th>
                          <th>Created at</th>
                          <th>Updated at</th>
                        </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th>Edit</th>
                          <th>On top</th>
                          <th>Title</th>
                          <th>Code</th>
                          <th>Status</th>
                          <th>Used</th>
                          <th>Last used</th>
                          <th>Success rate</th>
                          <th>Created at</th>
                          <th>Updated at</th>
                        </tr>
                      </tfoot>
                      <tbody>
                        @forelse ($company->coupons as $coupon)
                        <tr>
                          <td><a class="btn btn-primary btn-round" href="{{route('coupons.show', $coupon->id)}}">edit</a></td>
                          <td>
                              @if($coupon->top_choice)<i class="fas fa-check"></i>@else - @endif
                          </td>
                          <td>{{$coupon->title}}</td>
                          <td>{{$coupon->code}}</td>
                          <td>{{$coupon->humanStatus}}</td>
                          <td>{{$coupon->used}}</td>
                          <td>{{$coupon->last_used}}</td>
                          <td>%{{$coupon->success_rate}}</td>
                          <td>{{$coupon->created_at}}</td>
                          <td>{{$coupon->updated_at}}</td>
                        </tr>
                        @empty
                            {{-- No coupons --}}
                        @endforelse
                        
                      </tbody>
                    </table>
                  </div>
            </div>
              
            </div>
    </div>
</div>

@endsection