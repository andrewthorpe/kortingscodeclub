@extends('frontend.layouts.app')

@section('content')
<div class="page-margin">
    <div class="submit">
    <h1>Submit your coupon to be seen by our audience of millions of shoppers</h1>
    <p>
    Wethrift has helped <strong>millions of people save money</strong> when they shop online at stores like these:
    </p>
    <div class="submit-stores">
    <div class="submit-store">
    <img src="https://img.wethrift.com/frankie-rose-cosmetics.jpg" />
    </div>
    <div class="submit-store">
    <img src="https://img.wethrift.com/curves-n-combat-boots.jpg" />
    </div>
    <div class="submit-store">
    <img src="https://img.wethrift.com/monsterbass.jpg" />
    </div>
    <div class="submit-store">
    <img src="https://img.wethrift.com/myglamm.jpg" />
    </div>
    <div class="submit-store">
    <img src="https://img.wethrift.com/evive-smoothie.jpg" />
    </div>
    <div class="submit-store">
    <img src="https://img.wethrift.com/we-are-sneak.jpg" />
    </div>
    <div class="submit-store">
    <img src="https://img.wethrift.com/gopuff.jpg" />
    </div>
    <div class="submit-store">
    <img src="https://img.wethrift.com/bondi-boost.jpg" />
    </div>
    <div class="submit-store">
    <img src="https://img.wethrift.com/guts-and-gusto.jpg" />
    </div>
    <div class="submit-store">
    <img src="https://img.wethrift.com/the-kooples.jpg" />
    </div>
    </div>
    <p>
    Now, we want to help you promote your online store, service or coupon code to our audience of millions of shoppers and hundreds of thousands of subscribers.
    </p>
    <p>
    It only takes a second to get your coupon submtted to Wethrift.
    </p>
    Millons have saved
    tens of tounsands of stores
    Get your coupon code in front of
    <h2>How to submit a coupon to Wethrift</h2>
    <p>
    Submitting a coupon is easy. Simply send an email to <a href="mailto:submit@wethrift.com">submit@wethrift.com</a>. Make sure you include these details:
    </p>
    <ul class="submit-details">
    <li>
    Name of the store: <span>E.g. Nick's Bargain Shoe Shop</span>
    </li>
    <li>
    Store website URL: <span>E.g. www.example.com</span>
    </li>
    <li>
    Coupon code: <span>E.g. SAVE20</span>
    </li>
    <li>
    Coupon code value( $ or % off value): <span>E.g. 20% off</span>
    </li>
    <li>
    Coupon code description: <span>E.g. First orders only</span>
    </li>
    <li>
    Coupon expiry date: <span>E.g. December 31, 2099</span>
    </li>
    </ul>
    <div class="submit-cta">
    <a class="submit-button" href="mailto:submit@wethrift.com">
    Send an email to submit@wethrift.com
    </a>
    </div>
    <h2>How to submit your store to Wethrift</h2>
    <p>
    If you are the owner of an online store, and you would like to have your store listed on Wethrift, send us an emal to <a href="mailto:submit@wethrift.com">submit@wethrift.com</a>. Include the details of at least one coupon code (details above), along with the following:
    </p>
    <ul class="submit-details">
    <li>
    Name of your store: <span>E.g. Nick's Bargain Shoe Shop</span>
    </li>
    <li>
    Store website URL: <span>E.g. www.example.com</span>
    </li>
    <li>
    A short description of your store (10 - 30 words) - <span>E.g. "The best bargain shoe shop online"</span>
    </li>
    <li>
    Three categories that your store belongs to - <span>E.g. Shoes, Boots, Fashion</span>
    </li>
    </ul>
    <div class="submit-cta">
    <a class="submit-button" href="mailto:submit@wethrift.com">
    Send an email to submit@wethrift.com
    </a>
    </div>
    </div>
    </div>
@endsection
