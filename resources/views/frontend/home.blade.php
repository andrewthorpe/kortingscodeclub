@extends('frontend.layouts.app') @section('content') {{-- $companies --}}

<section class="wwd">
    <div class="page-margin">
        <h1>kortingscodeclub.nl helpt winkelaars maandelijk miljoenen euros te besparen bij meer dan 100,000 winkels.</h1>
    </div>
</section>
{{-- <section class="ext">
    <div class="page-margin">
        <div class="ext-panels">
            <div class="ext-panels-left">
                <img src="//i.imgur.com/ITleSVf.png" />
            </div>
            <div class="ext-panels-right">
                <h2>Never search for a coupon again with the Wethrift Chrome extension</h2>
                <div class="ext-feature">
                    <span>Automatically finds coupons while you shop</span>
                </div>
                <div class="ext-feature">
                    <span>Save at 3x more stores than Honey</span>
                </div>
                <div class="ext-feature">
                    <span>Saves more than any other coupon extension</span>
                </div>
                <div class="ext-link-row">
                    <span>See how the <a
              href="https://www.wethrift.com/money-saving-extension">top 12
              money-saving Chrome extensions compare.</a></span>
                </div>
                <a href="https://chrome.google.com/webstore/detail/wethrift-coupons-promos-d/ekiobbppdhfhgkhkemlhfgiobbjioklf"><img src="//i.imgur.com/0JPIcng.png" class="ext-web-store" /></a>
            </div>
        </div>
    </div>
</section> --}}
<section>
    <div class="section-content">
        <h1>Coupens en kortings codes van onze favoriete winkels.</h1>

        <div class="related-stores">
            @forelse ($top_companies as $company)

            <a class="related-store" href="{{route('company', $company->id)}}">
                <img class="related-store-logo" src="#" />
                <div class="related-store-info">
                    <div class="related-store-title">
                        <span class="related-store-name">{{ $company->name }}</span>
                    </div>
                    <div class="related-store-domain">
                        {{ $company->strippedUrl }}
                    </div>
                </div>
            </a>

            @empty {{-- no top companies --}} @endforelse


        </div>
    </div>
</section>
<section>
    <div class="section-content">
        <h1>Vandaags beste coupons en kortings codes</h1>

        <div class="coupon-list-container">

        @forelse ($top_coupons as $coupon) 
            
            {{-- On click, open popup (acording to name and code) --}}
            <div class="coupon-list-row" onclick="couponClick('{{$coupon->company->id}}', '{{$coupon->id}}')">
                <div class="coupon-info-container">
                    <img class="coupon-logo" src="#" />
                        <a class="coupon-info mob-order-1" href="{{route('company', $coupon->company->id)}}">
                        <div class="coupon-heading">
                            Coupon Code
                        </div>
                        <div class="coupon-line-1">{{$coupon->title}}</div>
                        <div class="coupon-domain">@ <strong>{{$coupon->company->strippedUrl}}</strong></div>
                    </a>
                </div>
                <div class="coupon-feature mob-order-3">
                    <div class="coupon-feature-title">Ondekt</div>
                    <div class="coupon-feature-value">{{$coupon->CreatedAtTimeDiff}}</div>
                </div>
                <div class="coupon-feature mob-order-4">
                    <div class="coupon-feature-title">Coupon gebruikt</div>
                    <div class="coupon-feature-value">
                        {{$coupon->used}} keer
                    </div>
                </div>
                <div class="coupon-feature mob-order-4">
                    <div class="coupon-feature-title">Laats gebruikt</div>
                    <div class="coupon-feature-value">
                        {{$coupon->LastUsedTimeDiff}}
                    </div>
                </div>
                <div class="coupon-feature mob-order-5">
                    <div class="coupon-feature-title">Success rate</div>
                    <div class="coupon-feature-value">
                        {{$coupon->success_rate}}%
                    </div>
                </div>
                <div class="coupon-button mob-order-2">
                    <div class="coupon-button-title">Copy code</div>
                    <div class="coupon-button-code">{{$coupon->code}}</div>
                </div>
            </div>
            @empty 
                {{-- no top coupons --}} 
            @endforelse
        </div>
</section>


<section>
    <div class="section-content">
        <h1>Vind meer top coupons van onze beste stores.</h1>
        <div class="related-stores">

            @forelse ($companies as $company)
                <a class="related-store" href="{{route('company', $company->id)}}">
                    <img class="related-store-logo" src="//img.wethrift.com/frankie-rose-cosmetics.jpg" />
                    <div class="related-store-info">
                        <div class="related-store-title">
                            <span class="related-store-name">{{ $company->name }}</span>
                        </div>
                        <div class="related-store-domain">
                            {{ $company->strippedUrl }}
                        </div>
                    </div>
                </a>
            @empty
                {{-- No other companies --}}
            @endforelse
        </div>

        <button id="btn">clickmebitch</button>

    </div>
</section>
@endsection