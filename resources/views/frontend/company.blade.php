@extends('frontend.layouts.app')

@section('content')

{{-- popup modal --}}
{{-- @include('frontend.modal') --}}
<!-- Modal -->
<div class="modal" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <img class="modal-logo" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D" /> <!-- this is a 1px transparent GIF -->
          <div class="modal-header-info">
              <div class="modal-title"></div>
              <div class="modal-desc"></div>
              <div class="modal-site"></div>
          </div>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="modal-body-title">Copy the code to claim your discount</div>
          <div class="modal-copy-container">
              <input class="modal-copy-input" id="modal-copy-code" readonly="readonly">
              <button class="modal-copy-button" id="modal-copy-button">COPY</button>
          </div>
        </div>
        <div class="modal-footer">
          <div class="modal-body-title">Help us improve this offer</div>
          <div class="modal-feedback-container">
              <div class="modal-feedback-question q1">
                  <div class="modal-feedback-question-question">Did this code work?</div>
                  <div class="modal-feedback-question-answers">
                      <div class="modal-feedback-question-answer yes"><div class="feedback-answer-icon">✓</div><div class="feedback-answer-text">Yes</div></div>
                      <div class="modal-feedback-question-answer no"><div class="feedback-answer-icon">→</div><div class="feedback-answer-text">No</div></div>
                  </div>
              </div>
              <div class="modal-feedback-question q2">
                  <div class="modal-feedback-question-question">How much did you save?</div>
                  <div class="modal-feedback-question-answers">
                      <input class="modal-feedback-input" id="modal-feedback-saving">
                      <button class="modal-feedback-submit" id="modal-feedback-submit">Next</button>
                  </div>
                  <div class="modal-feedback-skip">Skip</div>
              </div>
              <div class="modal-feedback-question q3">
                  <div class="modal-feedback-question-question">Does this store have free shipping?</div>
                  <div class="modal-feedback-question-answers">
                      <div class="modal-feedback-question-answer yes"><div class="feedback-answer-icon">✓</div><div class="feedback-answer-text">Yes</div></div>
                      <div class="modal-feedback-question-answer no"><div class="feedback-answer-icon">→</div><div class="feedback-answer-text">No</div></div>
                      <div class="modal-feedback-question-answer unsure"><div class="feedback-answer-icon">?</div><div class="feedback-answer-text">Not Sure</div></div>
                  </div>
              </div>
              <div class="modal-feedback-question q4">
                  <div class="modal-feedback-question-question">Does this store have free returns?</div>
                  <div class="modal-feedback-question-answers">
                      <div class="modal-feedback-question-answer yes"><div class="feedback-answer-icon">✓</div><div class="feedback-answer-text">Yes</div></div>
                      <div class="modal-feedback-question-answer no"><div class="feedback-answer-icon">→</div><div class="feedback-answer-text">No</div></div>
                      <div class="modal-feedback-question-answer unsure"><div class="feedback-answer-icon">?</div><div class="feedback-answer-text">Not Sure</div></div>
                  </div>
              </div>
              <div class="modal-feedback-question q5">
                  <div class="modal-feedback-question-question">Does this store offer free international shipping?</div>
                  <div class="modal-feedback-question-answers">
                      <div class="modal-feedback-question-answer yes"><div class="feedback-answer-icon">✓</div><div class="feedback-answer-text">Yes</div></div>
                      <div class="modal-feedback-question-answer no"><div class="feedback-answer-icon">→</div><div class="feedback-answer-text">No</div></div>
                      <div class="modal-feedback-question-answer unsure"><div class="feedback-answer-icon">?</div><div class="feedback-answer-text">Not Sure</div></div>
                  </div>
              </div>
              <div class="modal-feedback-question done">
                  <div class="modal-feedback-question-question">Thank you</div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
    <div class="starter-template">
      <h1>Modal Example</h1>
      <p class="lead"><a type="button" href="#deal1" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-dealid="deal1">Click me!</a> <a href="#deal2" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-dealid="deal2">Or me!</a></p>
    </div>

<section class="section-light section-first">
    

    <div class="section-content">
    <h1>Our top Sakara coupons & promo codes</h1>
    <div class="coupon-list-container">
    
    @forelse ($company->getTopCoupons(2) as $coupon)

    {{-- Open coupon popup based on Code --}}
    <div class="coupon-list-row coupon-list-row-active" onClick="dealClick('F368WYTEP')">

        <div class="coupon-info-container">
        <img class="coupon-logo" src="//img.wethrift.com/sakara-life.jpg" />
        <h2 class="coupon-info mob-order-1">

        <span class="coupon-heading">
            {!! $coupon->renderCouponStatus($coupon->status) !!}
        </span>

    <span class="coupon-line-1">{{ $coupon->title }}</span>
        <span class="coupon-domain">at {{ $coupon->company->strippedUrl }}</span>
        </h2>
        </div>
        <div class="coupon-feature  mob-order-3">
        <div class="coupon-feature-title">Discovered</div>
        <div class="coupon-feature-value">{{ $coupon->CreatedAtTimeDiff }}</div>
        </div>
        <div class="coupon-feature mob-order-4">
        <div class="coupon-feature-title">Coupon used</div>
        <div class="coupon-feature-value">
            {{ $coupon->used }} times
        </div>
        </div>
        <div class="coupon-feature mob-order-4">
        <div class="coupon-feature-title">Last used</div>
        <div class="coupon-feature-value">
            {{ $coupon->lastUsedTimeDiff }}
        </div>
        </div>
        <div class="coupon-feature  mob-order-5">
        <div class="coupon-feature-title">Success rate</div>
        <div class="coupon-feature-value">
            {{ $coupon->success_rate }}%
        </div>
        </div>
        <div class="coupon-button-container mob-order-2">
        {{-- <div class="coupon-button-flag" id="test1">
            Our best coupon!
        </div> --}}
        <div class="coupon-button">
         <div class="coupon-button-code">Bekijk</div>
        </div>
        {{-- <div class="coupon-button-sub" id="test2">
            $48.75 average saving
        </div> --}}
        </div>
        </div> 
    @empty
        
    @endforelse


    

    <div class="ext-row ext-row-show">
    <div class="ext-mockup-container">
    <div class="ext-mockup">
    <img class="ext-logo" src="//img.wethrift.com/sakara-life.jpg" />
    <div class="ext-details">
    <div class="ext-line-1">
    <strong>{{ count($company->coupons) }} coupons gevonden</strong> bij
    </div>
    <div class="ext-line-2">
        {{ $company->name }}
    </div>
    <div class="ext-line-3">
        {{ $company->coupons->first()->title }}
    </div>
    </div>
    </div>
    </div>
    <div class="ext-copy">
    <div class="ext-copy-1">
    Wil jij een nieuwe coupon delen?
    </div>
    <div class="ext-copy-2">
    Neem dan contact met ons op.
    </div>
    </div>
    <div class="ext-action">
    <a target="_blank" href="#" onclick="#">CONTACT</a>
    </div>
    </div> 

    <div class="coupon-list-footer">
    <a href="#more">Bekijk {{ count($company->activeCoupons) }} meer actieve {{ $company->name }} coupons</a>
    </div>
    </div>
    </div>

    </section>


    <section class="section-store-info">

    <div class="section-content">
    
    <div class="store-info">
    <div class="store-info-panel">
    <div class="store-info-section">
    <h2 class="store-summary-title">
    Onze {{ $company->name }} Coupons, Promos and Kortings Codes
    </h2>
    <div class="store-summary-table-container">
    <table class="store-summary-table">
    <tbody>
    <tr class="store-summary-row">
    <td class="store-summary-row-label">
    🚚 Gratis retour:
    </td>
    <td class="store-summary-row-value">

    @if($company->free_returns)
        Yes
    @else
        No
    @endif

    </td>
    </tr>
    <tr class="store-summary-row">
    <td class="store-summary-row-label">
    📦 Gratis verzending:
    </td>
    <td class="store-summary-row-value">

    @if($company->free_shipping)
        Yes
    @else
        No
    @endif

    </td>
    </tr>

    <tr class="store-summary-row">
        <td class="store-summary-row-label">
            📦 Internationale verzending:
        </td>
        <td class="store-summary-row-value">
    
        @if($company->free_shipping)
            Yes
        @else
            No
        @endif
    
        </td>
    </tr>


    <tr class="store-summary-row">
    <td class="store-summary-row-label">
    💰 Gemiddelde besparing:
    </td>
    <td class="store-summary-row-value">
    €{{ $company->average_saving }}
    </td>
    </tr>

    <tr class="store-summary-row">
    <td class="store-summary-row-label">
    🏷 Beschikbare coupons:
    </td>
    <td class="store-summary-row-value">
    {{ count($company->coupons) }}
    </td>
    </tr>

    <tr class="store-summary-row">
    <td class="store-summary-row-label">
    🤑 Nieuwe coupons:
    </td>
    <td class="store-summary-row-value">
    {{ count($company->newCoupons) }}
    </td>
    </tr>
    <tr class="store-summary-row">
    <td class="store-summary-row-label">
    🙌 Best coupon:
    </td>
    <td class="store-summary-row-value">
    {{ $company->mostUsedCoupon->title }}
    </td>
    </tr>
    <tr class="store-summary-row">
    <td class="store-summary-row-label">
    ⏰ Last updated:
    </td>
    <td class="store-summary-row-value">
    {{ $company->lastUpdatedTimeDiff }}
    </td>
    </tr>
    </tbody>
    </table>
    </div>


    <div class="store-summary-links">
    <ul>
    <li>
    <a href="#more">{{ count($company->activeCoupons) }} actieve {{ $company->name }} promo codes</a>
    </li>
    <li>
    <a href="#expired" onClick="goToExpired()">{{ count($company->oldAndExpiredCoupons) }} oude & vervallen {{ $company->name }} kortingen</a>
    </li>

        {{-- render QnA list items (as links) --}}
        {!! \FAQ::renderQnALinks() !!}

    </ul>
    </div>
    

    </div>
    </div>
    <div class="store-info-panel">
    <div class="store-info-section">
    <h3 class="store-summary-headline">Over {{ $company->name }}</h3>
    <img class="store-info-logo" src="//img.wethrift.com/sakara-life.jpg" />
    <div class="store-info-name">{{ $company->name }}</div>
    <div class="store-info-domain"><a href="{{ $company->url }}" rel="nofollow" target="_blank" onclick="storeLinkClick()">{{ $company->strippedUrl }}</a></div>
    <div class="store-info-tags">
    {{-- <a href="/tag/organic"><div class="store-info-tag">organic</div></a>
    <a href="/tag/food"><div class="store-info-tag">food</div></a>
    <a href="/tag/delivery"><div class="store-info-tag">delivery</div></a> --}}
    </div>
    <div class="store-info-description">
    <p>
    {{ $company->about }}
    </p>
    </div>
    <h3 class="store-summary-headline">Over {{ $company->name }} coupons</h3>
    <div class="store-summary-description">
    <p>
    Wij hebben {{ count($company->coupons) }} coupons en kortings codes die jou zullen helpen in het besparen bij {{ $company->name }}. <strong>Onze beste meest populaire korting is {{ $company->mostUsedCoupon->title }}</strong>. We hebben ook de volgende deals ontdekt: {{ $company->randomCoupon->title }}.
    </p>
    <p>
    <strong>Onze laatste kortings code is ontdekt op {{ $company->lastDiscoveredCoupon }}</strong>. Gemiddeld <strong>ontdekken elke {{ $avg_discover_days }} dagen een nieuwe {{ $company->name }} kortings code</strong>. In de laatste 30 dagen heeft de kortingscodeclub.nl {{ count($company->getCouponsFoundInPeriod(30)) }} nieuwe {{ $company->name }} coupons gevonden.
    </p>
    <p>
        Recent hebben winkelaars <strong>gemiddeld €{{ $company->average_saving }} bespaard</strong> door het gebruiken van onze coupons bij {{ $company->name }}.
    </p>
    </div>
    </div>
    
    </div>
    </div> 
    </div>
    </section>
    <section class="section-light" id="more">
    <div class="section-content">
    
    <div style="margin-bottom: 30px" data-fuse="21855715020"></div>
    <h2>Meer {{ $company->strippedUrl }} coupons & kortings codes</h2>
    <div class="coupon-list-container">
    <div class="coupon-list-heading">
    {{ count($company->activeCoupons) }} actieve coupon codes bij {{ $company->name }}
    </div>    
    

    
    @forelse ($company->activeCoupons as $coupon)
    
    <div class="coupon-list-row coupon-list-row-active" onClick="dealClick('{{$coupon->code}}')">
        <div class="coupon-info-container">
        <img class="coupon-logo" src="//img.wethrift.com/sakara-life.jpg" />
        <h2 class="coupon-info mob-order-1">
        <span class="coupon-heading">{{$coupon->company->name}} Coupon</span>
        <span class="coupon-line-1">{{$coupon->title}}</span>
        <span class="coupon-line-2">{{$coupon->company->strippedUrl}}</span>
        </h2>
        </div>
        <div class="coupon-feature  mob-order-3">
        <div class="coupon-feature-title">Ondekt</div>
        <div class="coupon-feature-value">{{$coupon->CreatedAtTimeDiff}}</div>
        </div>
        <div class="coupon-feature mob-order-4">
        <div class="coupon-feature-title">Coupon gebruikt</div>
        <div class="coupon-feature-value">
            {{$coupon->used}} keer
        </div>
        </div>
        <div class="coupon-feature mob-order-4">
        <div class="coupon-feature-title">Laats gebruikt</div>
        <div class="coupon-feature-value">
            {{$coupon->LastUsedTimeDiff}}
        </div>
        </div>
        <div class="coupon-feature  mob-order-5">
        <div class="coupon-feature-title">Success rate</div>
        <div class="coupon-feature-value">
            {{$coupon->success_rate}}%
        </div>
        </div>
        <div class="coupon-button-container mob-order-2">
            <div class="coupon-button">
            <div class="coupon-button-code">Bekijk</div>
            </div>
        </div>
        </div> 
    @empty
        {{-- Geen actieve coupons --}}
    @endforelse
    

    <div class="coupon-list-heading " id="expired">
        {{ count($company->oldAndExpiredCoupons) }}  oude & vervallen kortingen voor {{ $company->name }}
    </div>
    
    <div style="border-bottom: 1px solid #e4e8ee;" data-fuse="21855715023"></div>

    @forelse ($company->oldAndExpiredCoupons as $coupon)
        <div class="coupon-list-row coupon-list-row-expired" onClick="dealClick('FKUJBJUDP')">
        <div class="coupon-info-container">
            <img class="coupon-logo" src="//img.wethrift.com/sakara-life.jpg" />
            <h2 class="coupon-info mob-order-1">
            <span class="coupon-heading">{{$coupon->company->name}} Coupon</span>
            <span class="coupon-line-1">{{$coupon->title}}</span>
            <span class="coupon-line-2">{{$coupon->company->strippedUrl}}</span>
            </h2>
            </div>
            <div class="coupon-feature  mob-order-3">
            <div class="coupon-feature-title">Ondekt</div>
            <div class="coupon-feature-value">{{$coupon->CreatedAtTimeDiff}}</div>
            </div>
            <div class="coupon-feature mob-order-4">
            <div class="coupon-feature-title">Coupon gebruikt</div>
            <div class="coupon-feature-value">
                {{$coupon->used}} keer
            </div>
            </div>
            <div class="coupon-feature mob-order-4">
            <div class="coupon-feature-title">Laats gebruikt</div>
            <div class="coupon-feature-value">
                {{$coupon->LastUsedTimeDiff}}
            </div>
            </div>
            <div class="coupon-feature  mob-order-5">
            <div class="coupon-feature-title">Success rate</div>
            <div class="coupon-feature-value">
                {{$coupon->success_rate}}%
            </div>
            </div>
            <div class="coupon-button-container mob-order-2">
                <div class="coupon-button">
                <div class="coupon-button-code">Bekijk</div>
                </div>
            </div>
    </div> 
    @empty
        {{-- No old and expired coupons --}}
    @endforelse
    
    {{-- UI TODO: hide x amount of coupons behind this click --}}
    {{-- <div class="coupon-list-footer" id="load-more-expired">
    <a onClick="loadMoreExpiredCoupons()">Load more Sakara coupons</a>
    </div> --}}

    </div>
    </div>
    </section>

    <section>
    <div class="section-content">
    
    {{-- Schema for google Q&A search result block --}}
    {!! \FAQ::renderSchema() !!}

        <div class="faq">
            {{-- render QnAs as html --}}
            {!! \FAQ::renderQnAs() !!}
        
        </div> 
    </div>
    </section>


    <section class="section-light">
    <div class="section-content">
    <h2>Vind coupons bij andere winkels vergelijkbaar met {{ $company->name }}</h2>
    <div class="related-stores">

    @forelse ($company->getCompanies(9) as $other_company)
        <a class="related-store" href="{{route('company', $other_company->id)}}">
        <img class="related-store-logo" src="//img.wethrift.com/thefloristmarket.jpg" />
        <div class="related-store-info">
        <div class="related-store-title">
        <span class="related-store-name">{{$other_company->name}}</span> coupons
        </div>
        <div class="related-store-domain">
            {{ $other_company->strippedUrl }}
        </div>
        </div>
        </a>
    @empty
        {{-- No other companies available --}}
    @endforelse

    </div>
    </section>
@endsection
