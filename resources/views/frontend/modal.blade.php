<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
	<script>
	// JSON offer info
	let dealInfo = [
	{"id":"deal1", "title":"Example Offer", "desc": "10% off your first order", "code":"10PCOFF", "site": "google.com", "icon": "https://logo.clearbit.com/foodhub.co.uk"},
	{"id":"deal2", "title":"Another Example Offer", "desc": "Free delivery", "code":"FREEDELIVERY", "site": "bing.com", "icon": "https://img.wethrift.com/mixtiles.jpg"}
	];
	let dealIdx = null; // Index of last offer viewed
	
	// Copy to clipboard
	function copyCode(event) {
		$(event.currentTarget).html('Copied')
		$('#modal-copy-code').select()
		document.execCommand("copy")
	}
	
	// Find offer by ID and display it
	function showDeal(id, modal) {
		if (id === undefined)
			return
		dealIdx = dealInfo.findIndex(function(e) { if (e.id == id) return true })
		if (dealIdx != -1) {
			showDealFromIndex(dealIdx, modal)
		}
	}
	
	// Set fields in modal
	function showDealFromIndex(idx, modal) {
		var deal = dealInfo[dealIdx]
		
		window.location.hash = deal.id
		
		modal.find('#modal-copy-button').html('Copy')
		modal.find('.modal-header .modal-logo').attr('src', deal.icon)
		modal.find('.modal-header .modal-title').html(deal.title)
		modal.find('.modal-header .modal-desc').html(deal.desc)
		modal.find('.modal-header .modal-site').html(deal.site)
		modal.find('#modal-copy-code').val(deal.code)
		modal.find('.modal-feedback-question').hide();
		modal.find('.modal-feedback-question.q1').first().show();
		modal.modal('show')
	}
	
	// Find and display next offer
	function nextDeal() {
		dealIdx++
		if (dealIdx == dealInfo.length)
			dealIdx = 0
		showDealFromIndex(dealIdx, $('#exampleModal'))
	}
	
	// Handle URL hash change
	function hashChangeHandler() {
		var hash = window.location.hash;
		if (hash[0] != '#')
			return
		hash = hash.slice(1)
		showDeal(hash, $('#exampleModal'))
	}
	
	$(function() {
		// Button click handlers
		$('#modal-copy-button').on('click', function(event) { copyCode(event) } )
		$('.modal-feedback-question.q1 .no').on('click', function() { nextDeal() } )
		$('.modal-feedback-question.q1 .yes').on('click', function() { $('.modal-feedback-question.q1').hide(); $('.modal-feedback-question.q2').show(); } )
		$('.modal-feedback-question.q2 .modal-feedback-submit, .modal-feedback-question.q2 .modal-feedback-skip').on('click', function() { $('.modal-feedback-question.q2').hide(); $('.modal-feedback-question.q3').show(); } )
		$('.modal-feedback-question.q3 .yes, .modal-feedback-question.q3 .no, .modal-feedback-question.q3 .unsure').on('click', function() { $('.modal-feedback-question.q3').hide(); $('.modal-feedback-question.q4').show(); } )
		$('.modal-feedback-question.q4 .yes, .modal-feedback-question.q4 .no, .modal-feedback-question.q4 .unsure').on('click', function() { $('.modal-feedback-question.q4').hide(); $('.modal-feedback-question.q5').show(); } )
		$('.modal-feedback-question.q5 .yes, .modal-feedback-question.q5 .no, .modal-feedback-question.q5 .unsure').on('click', function() { $('.modal-feedback-question.q5').hide(); $('.modal-feedback-question.done').show(); } )
		// When modal is displayed, fill with offer info
		$('#exampleModal').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget) // Triggering button
			var id = button.data('dealid') // Get data-dealID
			showDeal(id, $(this))
		});
		
		// On URL hash change
		$(window).on('hashchange', function() { hashChangeHandler() })
		// Not called by default
		hashChangeHandler()
	});
	</script>
	<style>
	body { padding-top: 5rem; }
	.starter-template { padding: 3rem 1.5rem; text-align: center; }
	.modal-header, .modal-footer { flex-wrap: wrap; border: none; }
	.modal-content { border: none; border-radius: 8px; box-shadow: rgba(19,41,104,.2) 0 2px 5px 0; }
	.modal-logo { height: 55px; width: 55px; margin-right: 8px; border-radius: 50%; }
	.modal-body-title, .modal-feedback-question-question, .modal-feedback-skip { text-align: center; flex: 100%; }
	.modal-copy-container { display: flex; }
	.modal-copy-input, .modal-feedback-input { border: 2px solid #000; border-radius: 7px 0 0 7px; border-right: 0; text-align: center; font-size: 24px; width: 75%; flex-basis: 75%; }
	.modal-feedback-input { margin: 0 0 0 10px; }
	.modal-copy-input:focus, .modal-copy-button:focus, .modal-feedback-input:focus, .modal-feedback-submit:hover { outline: none; }
	.modal-copy-button, .modal-feedback-submit { font-size: 16px; background-color: #7d64dc; border: 2px solid #000; border-left: none; border-radius: 0 7px 7px 0; color: #fff; outline: 0; width: 25%; flex-basis: 25%; text-transform: uppercase; cursor: pointer; }
	.modal-feedback-submit { background-color: #f1f2f6; color: #565656; margin: 0 10px 0 0; }
	.modal-feedback-container { flex: 100%; }
	.modal-feedback-question { display: none; border: 4px solid green; border-radius: 8px; display: flex; flex-direction: column; flex-wrap: wrap; }
	.modal-feedback-question-answers { display: flex; flex-basis: 100%; }
	.modal-feedback-question-answer:hover { background: #ccc; cursor: pointer; }
	.modal-feedback-question-answer { flex: 1; flex-direction: column; align-items: center; justify-content: center; display: flex; padding: 5px 0; }
	.modal-feedback-question-answer .feedback-answer-icon { font-size: 2rem; border: 4px solid; width: 2.5rem; height: 2.5rem; text-align: center; line-height: 28px; border-radius: 50%; }
	.modal-feedback-question-answer.yes:hover { color: green; }
	.modal-feedback-question-answer.no:hover { color: red; }
	.modal-feedback-question-answer.unsure:hover { color: orange; }
	.modal-feedback-question-answer.no .feedback-answer-icon { line-height: 22px; }
	.modal-feedback-question-answer .feedback-answer-text { flex: 1; }
	.modal-feedback-skip { cursor: pointer; }
	</style>
</head>
<body>


<main role="main" class="container">

<div class="modal" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
	  <div class="modal-header">
		<img class="modal-logo" src="data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs%3D" /> <!-- this is a 1px transparent GIF -->
		<div class="modal-header-info">
			<div class="modal-title"></div>
			<div class="modal-desc"></div>
			<div class="modal-site"></div>
		</div>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		</button>
	  </div>
      <div class="modal-body">
        <div class="modal-body-title">Copy the code to claim your discount</div>
		<div class="modal-copy-container">
			<input class="modal-copy-input" id="modal-copy-code" readonly="readonly">
			<button class="modal-copy-button" id="modal-copy-button">COPY</button>
		</div>
      </div>
      <div class="modal-footer">
		<div class="modal-body-title">Help us improve this offer</div>
        <div class="modal-feedback-container">
			<div class="modal-feedback-question q1">
				<div class="modal-feedback-question-question">Did this code work?</div>
				<div class="modal-feedback-question-answers">
					<div class="modal-feedback-question-answer yes"><div class="feedback-answer-icon">✓</div><div class="feedback-answer-text">Yes</div></div>
					<div class="modal-feedback-question-answer no"><div class="feedback-answer-icon">→</div><div class="feedback-answer-text">No</div></div>
				</div>
			</div>
			<div class="modal-feedback-question q2">
				<div class="modal-feedback-question-question">How much did you save?</div>
				<div class="modal-feedback-question-answers">
					<input class="modal-feedback-input" id="modal-feedback-saving">
					<button class="modal-feedback-submit" id="modal-feedback-submit">Next</button>
				</div>
				<div class="modal-feedback-skip">Skip</div>
			</div>
			<div class="modal-feedback-question q3">
				<div class="modal-feedback-question-question">Does this store have free shipping?</div>
				<div class="modal-feedback-question-answers">
					<div class="modal-feedback-question-answer yes"><div class="feedback-answer-icon">✓</div><div class="feedback-answer-text">Yes</div></div>
					<div class="modal-feedback-question-answer no"><div class="feedback-answer-icon">→</div><div class="feedback-answer-text">No</div></div>
					<div class="modal-feedback-question-answer unsure"><div class="feedback-answer-icon">?</div><div class="feedback-answer-text">Not Sure</div></div>
				</div>
			</div>
			<div class="modal-feedback-question q4">
				<div class="modal-feedback-question-question">Does this store have free returns?</div>
				<div class="modal-feedback-question-answers">
					<div class="modal-feedback-question-answer yes"><div class="feedback-answer-icon">✓</div><div class="feedback-answer-text">Yes</div></div>
					<div class="modal-feedback-question-answer no"><div class="feedback-answer-icon">→</div><div class="feedback-answer-text">No</div></div>
					<div class="modal-feedback-question-answer unsure"><div class="feedback-answer-icon">?</div><div class="feedback-answer-text">Not Sure</div></div>
				</div>
			</div>
			<div class="modal-feedback-question q5">
				<div class="modal-feedback-question-question">Does this store offer free international shipping?</div>
				<div class="modal-feedback-question-answers">
					<div class="modal-feedback-question-answer yes"><div class="feedback-answer-icon">✓</div><div class="feedback-answer-text">Yes</div></div>
					<div class="modal-feedback-question-answer no"><div class="feedback-answer-icon">→</div><div class="feedback-answer-text">No</div></div>
					<div class="modal-feedback-question-answer unsure"><div class="feedback-answer-icon">?</div><div class="feedback-answer-text">Not Sure</div></div>
				</div>
			</div>
			<div class="modal-feedback-question done">
				<div class="modal-feedback-question-question">Thank you</div>
			</div>
		</div>
      </div>
    </div>
  </div>
</div>

  <div class="starter-template">
    <h1>Modal Example</h1>
    <p class="lead"><a type="button" href="#deal1" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-dealid="deal1">Click me!</a> <a href="#deal2" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-dealid="deal2">Or me!</a></p>
  </div>

</main>
</body>
</html>