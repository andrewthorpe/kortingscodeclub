@extends('frontend.layouts.app')

@section('content')

<section class="wwd">
<div class="page-margin">
<h1>Wethrift helps shoppers save millions of dollars every month at over 100,000 online stores.</h1>
</div>
</section>
<section class="awt">
<div class="page-margin">
<h1>About Wethrift</h1>
<p class="awt-text">
<strong>We help people save money when they shop online</strong> by finding and sharing the best coupons and promo codes, including many discounts that you won't find anywhere else. <strong>More than 900,000 shoppers</strong> have discovered new ways to save money at <strong>over 100,000 stores</strong> in the last month.
</p>
<p class="awt-text">
<span class="highlight">We only publish coupon codes.</span> We don't publish fake deals or offers that don't work. We only publish real, working coupon codes. We are honest and authentic and shoppers love us for it.
</p>
<p class="awt-text">
<span class="highlight">We work with stores</span> to bring shoppers the best deals at the stores they love.
</p>
</div>
</section>
<section class="wss">
<div class="page-margin">
<h1>What shoppers say about Wethrift</h1>
<div class="wss-quotes">
<div class="wss-quote">
<div class="wss-quote-text">
"OMG, This actually works! Had to go through 3 coupon codes for this particular store and then...BAM! One worked and I saved 15% off my purchase. Thank you sooooo much :-D"
</div>
<div class="wss-quote-author">
<span class="wss-quote-name">Kirra B</span> <span class="wss-quote-date">Nov 3, 2019</span> <span class="wss-quote-stars">★★★★★</span>
</div>
</div>
<div class="wss-quote">
<div class="wss-quote-text">
"Seriously, Honey gets all the fame but honestly I've found WeThrift to be MUCH more helpful, giving me codes for obscure websites. If I could give it six stars I would."
</div>
<div class="wss-quote-author">
<span class="wss-quote-name">Evan C</span> <span class="wss-quote-date">Oct 14, 2019</span> <span class="wss-quote-stars">★★★★★</span>
</div>
</div>
<div class="wss-quote">
<div class="wss-quote-text">
"Tried all the other ones... Honey, Groupon, etc. None of the codes ended up working or were expired. Tried this and got 10% off first try on my tea order. Awesome!!!"
</div>
<div class="wss-quote-author">
<span class="wss-quote-name">Valmir G</span> <span class="wss-quote-date">Oct 5, 2019</span> <span class="wss-quote-stars">★★★★★</span>
</div>
</div>
</div>
</div>
</section> 
<section class="par">
<div class="page-margin">
<h1>Partner with Wethrift</h1>
<div class="par-text">
<p>
Partner with us to extend your reach to our audience of <strong>over 900,000 qualified online shoppers</strong>, and our email database that reaches <strong>more than 200,000 subscribers</strong> who are waiting to receive their next deal.
</p>
<h2>Why partner with us?</h2>
<p>
<span class="highlight">Increase your conversion rate</span> by <a href="https://www.retailmenot.com/corp/gui/671c7d7/filer_public/0c/a8/0ca88815-8c20-4cdc-80db-28cb2dcab7d2/rmn_surprisingtrends_retail_wp.pdf" rel="nofollow">up to 33%</a> by integrating a coupon code into your next campaign. Shoppers love to get the best deal possible, and offering a coupon code is one of the best ways to accelerate the journey of your customers through your shopping cart.
</p>
<p>
<span class="highlight">Your competitors are already doing it.</span> We're already publishing coupon codes for over 100,000 online stores, and <a href="https://research.retailmenot.com/holidays2019/p/1" rel="nofollow">76% of stores</a> are planning on offerring more discounts in the next year than the year before.
</p>
<p>
<span class="highlight">Help your customers find what they are looking for.</span> 94% of shoppers <a href="https://www.inc.com/peter-roesler/new-study-shows-deals-promotions-affect-every-part-of-shopping-experience.html" rel="nofollow">search for deals</a> before buying online. You can gain greater control over the deals they see, and which products they end up purchasing.
</p>
<p>
If you're interested in a partnership, send an email to <a href="mailto:hello@wethrift.com">hello@wethrift.com</a> or view our <a href="/submit">submission page</a> for more information about getting your store listed on Wethrift.
</p>
<h2>Who shops with Wethrift?</h2>
<div id="visitors"></div>
<div class="par-metrics">
<div class="par-metric" id="age"></div>
<div class="par-metric" id="gender"></div>
</div>
</div>
</section>
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<script>
    // visitors
    var visOptions = {
      chart: {
        height: 450,
        type: 'area',
        toolbar: {
          show: false
        }
      },
      title: {
        text: "Monthly visitors",
        align: 'center',
        style: {
          fontSize: '20px'
        }
      },
      colors: ['#7d64dc'],
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: 'smooth'
      },
      series: [{
        name: 'Monthly visitors',
        data: [2643, 15070, 61993, 102439, 141309, 182752, 215517, 241849, 247738, 308387, 421525, 423843, 450367, 430807, 486539, 517074, 572846, 576981, 631551, 675987, 623624, 771158, 918480]
      }],
      xaxis: {
        type: 'date',
        categories: ["Jan-2018", "Feb-2018", "Mar-2018", "Apr-2018", "May-2018", "Jun-2018", "Jul-2018", "Aug-2018", "Sep-2018", "Oct-2018", "Nov-2018", "Dec-2018", "Jan-2019", "Feb-2019", "Mar-2019", "Apr-2019", "May-2019", "Jun-2019", "Jul-2019", "Aug-2019", "Sep-2019", "Oct-2019", "Nov-2019"],
      },
      yaxis: {
        min: 0, max: 1000000
      },
      tooltip: {
        x: {
          format: 'MMMM-yyyy'
        },
      }
    }
    
    var visChart = new ApexCharts(
      document.querySelector("#visitors"),
      visOptions
    )
    visChart.render()
    
    // age
    var ageOptions = {
      chart: {
        height: 450,
        type: 'bar',
        toolbar: {
          show: false
        }
      },
      colors: ['#7d64dc'],
      plotOptions: {
        bar: {
          dataLabels: {
            position: 'top' // top, center, bottom
          }
        }
      },
      dataLabels: {
        enabled: true,
        formatter: function(val) {
          return val + '%'
        },
        offsetY: -20,
        style: {
          fontSize: '12px',
          colors: ['#304758']
        }
      },
      series: [
        {
          name: 'Percentage',
          data: [14.79, 41.16, 22.21, 12.33, 6.45, 3.06]
        }
      ],
      xaxis: {
        categories: ['18-24', '25-34', '35-44', '45-54', '55-64', '65+'],
        position: 'bottom',
        labels: {
          // offsetY: -18
        },
        axisBorder: {
          show: false
        },
        axisTicks: {
          show: false
        },
        crosshairs: {
          fill: {
            type: 'gradient',
            gradient: {
              colorFrom: '#D8E3F0',
              colorTo: '#BED1E6',
              stops: [0, 100],
              opacityFrom: 0.4,
              opacityTo: 0.5
            }
          }
        },
        tooltip: {
          enabled: false,
          offsetY: -35
        }
      },
      fill: {
        gradient: {
          shade: 'light',
          type: 'horizontal',
          shadeIntensity: 0.25,
          gradientToColors: undefined,
          inverseColors: true,
          opacityFrom: 1,
          opacityTo: 1,
          stops: [50, 0, 100, 100]
        }
      },
      yaxis: {
        axisBorder: {
          show: false
        },
        axisTicks: {
          show: false
        },
        labels: {
          show: false,
          formatter: function(val) {
            return val + '%'
          }
        }
      },
      title: {
        text: 'Age of Wethrift.com visitors',
        floating: true,
        // offsetY: 50,
        align: 'center',
        style: {
          color: '#444',
          fontSize: '20px'
        }
      }
    }
    
    var ageChart = new ApexCharts(document.querySelector('#age'), ageOptions)
    ageChart.render()
    
    // Gender
    var genOptions = {
      chart: {
        width: '100%',
        type: 'pie',
        height: 450,
        toolbar: {
          show: false
        }
      },
      colors: ['#a79ada', '#7d64dc'],
      series: [36.9, 63.1],
      labels: ['Male', 'Female'],
      // theme: {
      //   monochrome: {
      //     enabled: true
      //   }
      // },
      title: {
        text: "Gender",
        align: 'center',
        style: {
          fontSize: '20px'
        }
      }
    }
    
    var genChart = new ApexCharts(document.querySelector('#gender'), genOptions)
    genChart.render()
    
    </script>

@endsection
