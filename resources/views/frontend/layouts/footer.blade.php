<footer class="page-footer">
    <div class="page-margin">
    <div class="footer-col">
    <div class="footer-col-heading footer-logo">
    <span class="footer-logo-we">we</span>thrift
    </div>
    <div class="footer-text">
    Find the best coupon codes, promos and discounts and save. Exclusive deals for the stores you love.
    </div>
    <ul>
    <li><a href="/about">About Wethrift</a></li>
    <li><a href="/privacy">Privacy policy</a></li>
    <li><a href="/terms">Terms and conditions</a></li>
    </ul>
    </div>
    <div class="footer-col">
    <div class="footer-col-heading">
    Top Categories
    </div>
    <ul>
    <li><a href="/tag/health">Health</a></li>
    <li><a href="/tag/fashion">Fashion</a></li>
    <li><a href="/tag/pets">Pets</a></li>
    <li><a href="/tag/clothing">Clothing</a></li>
    <li><a href="/tag/automotive">Automotive</a></li>
    <li><a href="/tag/fitness">Fitness</a></li>
    </ul>
    </div>
    <div class="footer-col">
    <div class="footer-col-heading">
    Discover Great Deals</br>Follow Wethrift
    </div>
    <ul class="social-links">
    <li>
    <a href="https://www.instagram.com/teamwethrift" onclick="externalLinkClick(event)" target="_blank">
    <img src="/img/icons/instagram-white.svg" />
    </a>
    </li>
    <li>
    <a href="https://twitter.com/teamwethrift" onclick="externalLinkClick(event)" target="_blank">
    <img src="/img/icons/twitter-white.svg" />
    </a>
    </li>
    <li>
    <a href="https://chrome.google.com/webstore/detail/wethrift/ekiobbppdhfhgkhkemlhfgiobbjioklf" onclick="externalLinkClick(event)" target="_blank">
    <img src="/img/icons/google-chrome.svg" />
    </a>
    </li>
    </ul>
    <ul>
    <li>
    <a href="https://chrome.google.com/webstore/detail/wethrift/ekiobbppdhfhgkhkemlhfgiobbjioklf">
    Get the Wethrift Chrome Extension
    </a>
    </li>
    </ul>
    </div>
    <div class="footer-col">
    <div class="footer-col-heading">
    Contact
    </div>
    <ul>
    <li><a href="/submit">Submit a coupon</a></li>
    </ul>
    <div class="footer-text">
    Wethrift.com</br>
    340 S LEMON AVE #1262</br>
    WALNUT, CA 91789, United States</br></br>
    <a href="mailto:hello@wethrift.com">hello@wethrift.com</a>
    </div>
    </div>
    </div>
    </footer> <script src="https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js"></script>
    <script src="https://cdn.jsdelivr.net/autocomplete.js/0/autocomplete.min.js"></script>
    <script src="/js-dist/search-min.js"></script>
        <script src="{{asset('js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>

    <script>
      function couponClick(company_id, deal_id){
        window.location = window.location.origin + '/company/' + company_id + '/#deal=' + deal_id
      }
    </script>
