<nav class="top-nav">
    <div class="page-margin nav-margin">
    <div class="logo">
    <a href="{{route('home')}}"><span class="we">Kortingscode</span>Club</a>
    </div>
    
    <div class="nav-icons">
    <div class="nav-icon nav-icon-search" onClick="showSearch()">
    <span class="vertical-align-helper"></span>
    <img class="nav-icon-img" id="search-img-open" src="/img/icons/search.svg" alt="open search icon" />
    <img class="nav-icon-img top-nav-items-search-show" id="search-img-close" src="/img/icons/x-black.svg" alt="close search icon" />
    </div>
    <div class="nav-icon nav-icon-menu" onClick="showMenu()">
    <span class="vertical-align-helper"></span>
    <img class="nav-icon-img" src="/img/icons/menu.svg" alt="menu icon" />
    </div>
    </div>
    </div>
    
    <div class="nav-centre">
    <ul class="top-nav-items">
    <li>
    <a href="{{route('submit')}}">Nieuwe Coupon</a>
    </li>
    <li>
    <a href="{{route('about')}}">Over ons</a>
    </li>
    </ul>
    
    <div class="nav-search search-box-hide">
    <form>
    <input type="text" id="search-input" placeholder="Search stores" />
    </form>
    </div>
    </div>
    </nav>