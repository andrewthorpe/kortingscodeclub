<script type="text/javascript" src="{{asset('js/head.js')}}"></script>
{{-- Conditionally add stylesheets, maybe frontend dev can refactor the sheets a bit? --}}
@switch(\Request::route()->getName())
    @case('home')
        <link rel="stylesheet" type="text/css" href="{{asset('css/home.css')}}">
        @break

    @case('about')
        <link rel="stylesheet" type="text/css" href="{{asset('css/about.css')}}">
        @break
    
    @case('submit')
        <link rel="stylesheet" type="text/css" href="{{asset('css/submit.css')}}">
        @break

    @case('company')
      <link rel="stylesheet" type="text/css" href="{{asset('css/company.css')}}">
      @break

    @default
      @break
@endswitch