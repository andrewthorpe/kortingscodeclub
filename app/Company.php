<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Coupon;
use Carbon\Carbon;

class Company extends Model
{
    protected $fillable = ['url', 'name', 'about', 'top_choice', 'free_returns', 'international_shipping', 'free_shipping', 'average_saving'];

    public function coupons()
    {
        return $this->hasMany('App\Coupon');
    }

    public function getTopCoupons($limit = 2)
    {
        return $this->coupons()
                    ->where('top_choice', true)
                    ->where('status', Coupon::STATUS_NEW)
                    ->orWhere('status', Coupon::STATUS_VERIFIED)
                    ->limit($limit)
                    ->get();
    }

    public function getMostUsedCouponAttribute()
    {
        return $this->coupons()->orderBy('used', 'desc')->first();
    }

    public function getActiveCouponsAttribute()
    {
        return $this->newCoupons->merge($this->verifiedCoupons);
    }

    public function getNewCouponsAttribute()
    {
        return $this->coupons->where('status', Coupon::STATUS_NEW);
    }

    public function getVerifiedCouponsAttribute()
    {
        return $this->coupons->where('status', Coupon::STATUS_VERIFIED);
    }

    public function getOldCouponsAttribute()
    {
        return $this->coupons->where('status', Coupon::STATUS_OLD);
    }

    public function getExpiredCouponsAttribute()
    {
        return $this->coupons->where('status', Coupon::STATUS_EXPIRED);
    }

    public function getOldAndExpiredCouponsAttribute()
    {
        return $this->oldCoupons->merge($this->expiredCoupons);
    }

    public function getTopCompanies($limit = 10)
    {
        return Company::where('top_choice', true)->limit($limit)->get();
    }

    public function getCompanies($limit = 10)
    {
        return Company::inRandomOrder()->limit($limit)->get();
    }

    public function getStrippedUrlAttribute()
    {
        return preg_replace("(^www.)", "", parse_url($this->url)['host'] );
    }

    public function getLastUpdatedTimeDiffAttribute()
    {
        return Carbon::createFromTimeStamp(strtotime($this->updated_at))->diffForHumans();
    }

    public function getRandomCouponAttribute()
    {
        return $this->coupons->random(1)->first();
    }

    public function getLastDiscoveredCouponAttribute()
    {
        return Carbon::parse($this->coupons()->orderBy('created_at', 'desc')->first()->created_at)->toFormattedDateString();
    }

    public function getCouponsFoundInPeriod($days)
    {

        $date_range = Carbon::now()->subDays($days);
        return $this->coupons('create_at', '<=', $date_range)->get();

    }

}
