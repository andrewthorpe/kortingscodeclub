<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Coupon extends Model
{

    protected $fillable = ['title', 'code', 'top_choice', 'status', 'used', 'success_rate'];

    const STATUS_NEW = 10;
    const STATUS_VERIFIED = 20;
    const STATUS_OLD = 30;
    const STATUS_EXPIRED = 40;


    public function company()
    {
        return $this->belongsTo('App\Company');
    }


    public function getTopCoupons($limit = 10)
    {
        return Coupon::inRandomOrder()->limit($limit)->get();
    }

    public function getCreatedAtTimeDiffAttribute()
    {
        return Carbon::createFromTimeStamp(strtotime($this->created_at))->diffForHumans();
    }

    public function getLastUsedTimeDiffAttribute()
    {
        return Carbon::createFromTimeStamp(strtotime($this->last_used))->diffForHumans();
    }

    public function getHumanStatusAttribute()
    {
        switch ($this->status) {
            case $this::STATUS_NEW:
                return 'NIEUW';
                break;
            
            case $this::STATUS_VERIFIED:
                return 'GEVERIFIEERD';
                break;

            case $this::STATUS_EXPIRED:
                return 'VERLOPEN';
                break;
            
            default:
                # code...
                break;
        }
    }

    public function renderCouponStatus($status_code)
    {
        switch ($status_code) {
            case $this::STATUS_NEW:
                return '<span class="coupon-heading-flag coupon-heading-flag-blue">NIEUW</span>';
                break;
            
            case $this::STATUS_VERIFIED:
                return '<span class="coupon-heading-flag coupon-heading-flag-green">GEVERIFIEERD</span>';
                break;

            case $this::STATUS_EXPIRED:
                return '<span class="coupon-heading-flag coupon-heading-flag-red">VERLOPEN</span>';
                break;
            
            default:
                # code...
                break;
        }
    }

}
