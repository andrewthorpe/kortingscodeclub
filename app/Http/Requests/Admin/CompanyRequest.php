<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'url' => 'required|string',
            'about' => 'required|string',
            'top_choice' => 'required|boolean',
            'free_returns' => 'required|boolean',
            'international_shipping' => 'required|boolean',
            'free_shipping' => 'required|boolean',
            'average_saving' => 'required|numeric'
        ];
    }
}