<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;

class CompanyController extends Controller
{

    public function show($id)
    {
        $company = Company::findOrFail($id);
        $avg_discover_days = rand(2,4);

        // load custom values for Q&A section
        $QnAs = \FAQ::loadCustomValues([
            '{{COMPANY_NAME}}' => $company->name,
            '{{NEW_COUPON_EVERY_X_DAYS}}' => $avg_discover_days,
            '{{NEW_COUPONS_COUNT_IN_LAST_X_DAYS}}' => count($company->getCouponsFoundInPeriod(30)),
            '{{MOST_POPULAIR_COUPON_TITLE}}' => $company->mostUsedCoupon->title,
            '{{USERS_SAVE_ON_AVERAGE_X}}' => $company->average_saving,
            '{{LAST_X_DAYS}}' => 30,
        ])->compose();

        \Meta::setTitle('KortingscodeClub.nl - Coupons & Discounts')->setDescription("Vind de beste coupon codes bij $company->name");
        
        return view('frontend.company', compact('company', 'avg_discover_days'));
    }
}
