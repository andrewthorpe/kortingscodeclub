<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Coupon;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $company = new Company;
        $coupon = new Coupon;

        $top_companies = $company->getTopCompanies(12);
        $companies = $company->getCompanies(12);
        $top_coupons = $coupon->getTopCoupons(12);

        return view('frontend.home', compact('top_companies', 'companies', 'top_coupons'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function about()
    {
        return view('frontend.about');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function submit()
    {
        return view('frontend.submit');
    }


}