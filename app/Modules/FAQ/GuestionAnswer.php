<?php

namespace App\Modules;

class GuestionAnswer
{
    public $question;
    public $answer;

    public function __construct($question, $answer)
    {
        $this->answer = $answer;
        $this->question = $question;
    }
}