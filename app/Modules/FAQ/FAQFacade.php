<?php

namespace App\Modules;
use Illuminate\Support\Facades\Facade;

class FAQFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'faq';
    }
}