<?php

namespace App\Modules;

use App\Modules\GuestionAnswer;
use Spatie\SchemaOrg\Schema;

class FAQ
{
    public $question_answeres = array();
    private $qa_values = array();

    public function __construct($answer_questions)
    {
        foreach($answer_questions as $question => $answer){
            array_push($this->question_answeres, new GuestionAnswer($question, $answer));
        }
    }

    public function loadCustomValues($values)
    {
        array_push($this->qa_values, $values);
        return $this;
    }

    public function compose()
    {
        foreach($this->question_answeres as $question_answere){

              // Replace words inside question 
              $question_answere->question = str_replace(
                array_keys($this->qa_values[0]),
                array_values($this->qa_values[0]), 
                $question_answere->question
              ); 

              // Replace words inside answer 
              $question_answere->answer = str_replace(
                array_keys($this->qa_values[0]), 
                array_values($this->qa_values[0]), 
                $question_answere->answer
              );
        }

        return $this;
    }

    public function getQnAs()
    {
        return $this->question_answeres;
    }


    public function renderQnAs()
    {


        if(count($this->question_answeres) > 0){
            $list = '';
            foreach ($this->question_answeres as $key => $QnA) {
                $list = $list . 
                "<li>
                    <div class='faq-item' id='$key'>
                        <h3 class='faq-title'>$QnA->question</h3>
                        <div class='faq-content'>
                            <p>$QnA->answer</p>
                        </div>
                    </div>
                </li>";
            }
            return $list;
        }else{
            // No questions.
        }
    }

    public function renderQnALinks()
    {
        if(count($this->question_answeres) > 0){
            $list = '';
            foreach ($this->question_answeres as $key => $QnA) {
                $list = $list . 
                "<li>
                <a href='#$key'>$QnA->question</a>
                </li>";
            }
            return $list;
        }else{
            // No questions.
        }
    }

    public function renderSchema()
    {

        $QnAs = array();
        foreach ($this->question_answeres as $question_answere) {
            array_push(
                $QnAs,
                Schema::Question()
                    ->name($question_answere->question)
                    ->acceptedAnswer(
                        Schema::Answer()->text($question_answere->answer)
                    )
            );
        }

        return Schema::FAQPage()->mainEntity($QnAs)->toScript();
    }

}