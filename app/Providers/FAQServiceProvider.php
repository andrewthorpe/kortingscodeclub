<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Modules\FAQ;

class FAQServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('faq',function(){
            return new FAQ(config('FAQ'));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
