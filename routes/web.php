<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();



Route::get('/', 'HomeController@index')->name('home');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/submit', 'HomeController@submit')->name('submit');

Route::get('/company/{id}', 'CompanyController@show')->name('company');

// Route::get('/test', function(){
//     return view('frontend.modal');
// });

use Spatie\SchemaOrg\Schema;

Route::get('/schema', function(){

    // load custom values for Q&A section
    FAQ::loadCustomValues([
        '{{COMPANY_NAME}}' => 'blabal',
        '{{NEW_COUPON_EVERY_X_DAYS}}' => 4,
        '{{LAST_X_DAYS}}' => 30,
        '{{NEW_COUPONS_COUNT_IN_LAST_X_DAYS}}' => 7,
        '{{MOST_POPULAIR_COUPON_TITLE}}' => 'Blabkads ysa dhuia',
        '{{USERS_SAVE_ON_AVERAGE_X}}' => 56.89
    ]);

    // try to render custom values into the Q&A's
    if($QnA = FAQ::compose()){

    }

    // $faq_schema = Schema::FAQPage()
    // ->mainEntity([
    //     Schema::Question()
    //     ->name('How often does Sakara release new coupon codes?')
    //     ->acceptedAnswer(
    //         Schema::Answer()->text("Lately we've discovered a new discount code from Sakara every 4 days. Over the last 30 days we've found 7 new coupons from Sakara.")
    //     ),
    //     Schema::Question()
    //     ->name('How much can I save by using a coupon at Sakara?')
    //     ->acceptedAnswer(
    //         Schema::Answer()->text("The best discount we've found is a code for 25% off. Wethrift shoppers save an average of $55.83 at checkout.")
    //     ),
    //     Schema::Question()
    //     ->name('How much can I save by using a coupon at Sakara?')
    //     ->acceptedAnswer(
    //         Schema::Answer()->text("The best discount we've found is a code for 25% off. Wethrift shoppers save an average of $55.83 at checkout.")
    //     )
    // ]);

    dd($faq_schema->toScript());
    
});