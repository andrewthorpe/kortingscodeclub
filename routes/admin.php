<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/

Route::get('/dashboard', function(){
    return redirect(route('companies.index'));
});

Route::prefix('dashboard')->group(function () {
    Route::resource('companies', 'Admin\CompanyController');
    Route::resource('coupons', 'Admin\CouponController');
});



// Route::get('/dashboard/companies', function(){
//     $companies = App\Company::all();
//     return view('dashboard.companies', compact('companies'));
// })->name('dashboard.companies');

// Route::get('/dashboard/company/{id}', function($id){
//     $company = App\Company::find($id);
//     return view('dashboard.company', compact('company'));
// })->name('dashboard.company');

// Route::get('/dashboard/coupon/{id}', function($id){
//     $coupon = App\Coupon::find($id);
//     return view('dashboard.coupon', compact('coupon'));
// })->name('dashboard.coupon');