// visitors
var visOptions = {
    chart: {
        height: 450,
        type: "area",
        toolbar: {
            show: false
        }
    },
    title: {
        text: "Monthly visitors",
        align: "center",
        style: {
            fontSize: "20px"
        }
    },
    colors: ["#7d64dc"],
    dataLabels: {
        enabled: false
    },
    stroke: {
        curve: "smooth"
    },
    series: [
        {
            name: "Monthly visitors",
            data: [
                2643,
                15070,
                61993,
                102439,
                141309,
                182752,
                215517,
                241849,
                247738,
                308387,
                421525,
                423843,
                450367,
                430807,
                486539,
                517074,
                572846,
                576981,
                631551,
                675987,
                623624,
                771158,
                918480
            ]
        }
    ],
    xaxis: {
        type: "date",
        categories: [
            "Jan-2018",
            "Feb-2018",
            "Mar-2018",
            "Apr-2018",
            "May-2018",
            "Jun-2018",
            "Jul-2018",
            "Aug-2018",
            "Sep-2018",
            "Oct-2018",
            "Nov-2018",
            "Dec-2018",
            "Jan-2019",
            "Feb-2019",
            "Mar-2019",
            "Apr-2019",
            "May-2019",
            "Jun-2019",
            "Jul-2019",
            "Aug-2019",
            "Sep-2019",
            "Oct-2019",
            "Nov-2019"
        ]
    },
    yaxis: {
        min: 0,
        max: 1000000
    },
    tooltip: {
        x: {
            format: "MMMM-yyyy"
        }
    }
};

var visChart = new ApexCharts(document.querySelector("#visitors"), visOptions);
visChart.render();

// age
var ageOptions = {
    chart: {
        height: 450,
        type: "bar",
        toolbar: {
            show: false
        }
    },
    colors: ["#7d64dc"],
    plotOptions: {
        bar: {
            dataLabels: {
                position: "top" // top, center, bottom
            }
        }
    },
    dataLabels: {
        enabled: true,
        formatter: function(val) {
            return val + "%";
        },
        offsetY: -20,
        style: {
            fontSize: "12px",
            colors: ["#304758"]
        }
    },
    series: [
        {
            name: "Percentage",
            data: [14.79, 41.16, 22.21, 12.33, 6.45, 3.06]
        }
    ],
    xaxis: {
        categories: ["18-24", "25-34", "35-44", "45-54", "55-64", "65+"],
        position: "bottom",
        labels: {
            // offsetY: -18
        },
        axisBorder: {
            show: false
        },
        axisTicks: {
            show: false
        },
        crosshairs: {
            fill: {
                type: "gradient",
                gradient: {
                    colorFrom: "#D8E3F0",
                    colorTo: "#BED1E6",
                    stops: [0, 100],
                    opacityFrom: 0.4,
                    opacityTo: 0.5
                }
            }
        },
        tooltip: {
            enabled: false,
            offsetY: -35
        }
    },
    fill: {
        gradient: {
            shade: "light",
            type: "horizontal",
            shadeIntensity: 0.25,
            gradientToColors: undefined,
            inverseColors: true,
            opacityFrom: 1,
            opacityTo: 1,
            stops: [50, 0, 100, 100]
        }
    },
    yaxis: {
        axisBorder: {
            show: false
        },
        axisTicks: {
            show: false
        },
        labels: {
            show: false,
            formatter: function(val) {
                return val + "%";
            }
        }
    },
    title: {
        text: "Age of Wethrift.com visitors",
        floating: true,
        // offsetY: 50,
        align: "center",
        style: {
            color: "#444",
            fontSize: "20px"
        }
    }
};

var ageChart = new ApexCharts(document.querySelector("#age"), ageOptions);
ageChart.render();

// Gender
var genOptions = {
    chart: {
        width: "100%",
        type: "pie",
        height: 450,
        toolbar: {
            show: false
        }
    },
    colors: ["#a79ada", "#7d64dc"],
    series: [36.9, 63.1],
    labels: ["Male", "Female"],
    // theme: {
    //   monochrome: {
    //     enabled: true
    //   }
    // },
    title: {
        text: "Gender",
        align: "center",
        style: {
            fontSize: "20px"
        }
    }
};

var genChart = new ApexCharts(document.querySelector("#gender"), genOptions);
genChart.render();
