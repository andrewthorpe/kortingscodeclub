function doA(a) {
    return JSON.parse(a);
}

function doB(b) {
    return decodeURIComponent(b);
}

function doC(c) {
    return atob(c.replace(/\&\#x3D;/g, ""));
}
const store = doA(doB(doC(payload)));
store.deals = store.deals.map((d, i) => ((d.index = i), d));
const { userAgent: userAgent } = navigator,
is_chrome = /Chrome\//.test(userAgent),
    is_desktop = /Macintosh|Windows/.test(userAgent),
    is_ios = /ipad|ipod|iphone/i.test(userAgent);
let dealToSelect,
    selectedDeal = null,
    didMonetisedExit = !1,
    qaIndex = 0,
    chromeAdDisplayed = !1;
const qaQuestions = [{
            index: 2,
            question: "Does this store have free shipping?",
            qid: "free_shipping"
        },
        {
            index: 3,
            question: "Does this store have free returns?",
            qid: "free_returns"
        },
        {
            index: 4,
            question: "Does this store offer international shipping?",
            qid: "intl_shipping"
        }
    ],
    hash = window.location.hash,
    hash_match = hash.match(/deal=(\w+)/);

function showMenu() {
    $$(".nav-centre").classList.toggle("mobile-menu-show"),
        $$(".nav-search").classList.toggle("search-box-hide");
}

function loadMoreActiveCoupons() {
    ($$("#load-more-active").hidden = !0),
    Array.from($$$(".coupon-list-row-active-hidden")).forEach(row =>
        row.classList.remove("coupon-list-row-active-hidden")
    );
}

function loadMoreExpiredCoupons() {
    ($$("#load-more-expired").hidden = !0),
    Array.from($$$(".coupon-list-row-expired-hidden")).forEach(row =>
        row.classList.remove("coupon-list-row-expired-hidden")
    );
}

function goToExpired() {
    loadMoreActiveCoupons(),
        loadMoreExpiredCoupons(),
        (location.hash = "#expired");
}

function dealClick(deal_id) {
    if (store.monetised_link && !didMonetisedExit) {
        const url = `${window.location.pathname}#deal=${deal_id}`;
        window.open(url, "_blank"), (window.location = store.monetised_link);
    } else if (!store.monetised_link) {
        const chromeAdLastShown = window.localStorage.getItem(
                "chromeAdLastShown"
            ),
            maxTimestamp = Date.now() - 31536e6,
            showChromeAd =
            is_chrome &&
            is_desktop &&
            (!chromeAdLastShown || chromeAdLastShown < maxTimestamp);
        if ((($$(".popup").style.display = "flex"), showChromeAd))
            return openChromeAd(), void(dealToSelect = deal_id);
    }
    selectDeal(deal_id);
}

function openChromeAd() {
    ($$("#modal-chrome").style.display = "block"),
    ($$("#modal-chrome-url").innerText = store.domain),
    window.localStorage.setItem("chromeAdLastShown", Date.now()),
        (chromeAdDisplayed = !0),
        dataLayer.push({
            event: "chromeExtensionAdPopup",
            action: "displayed"
        });
}

function closeChromeAd() {
    ($$("#modal-chrome").style.display = "none"),
    selectDeal(dealToSelect),
        (chromeAdDisplayed = !1);
}

function clickChromeAd() {
    closeChromeAd(),
        dataLayer.push({ event: "chromeExtensionAdPopup", action: "clicked" });
}
hash_match && hash_match[1] && selectDeal(hash_match[1]),
    store.monetised_link && dataLayer.push({ event: "monetisedStoreView" }),
    store.monetised_link || dataLayer.push({ event: "unmonetisedStoreView" });
const chromeModalEl = $$("#modal-chrome");

function selectDeal(id) {
    (selectedDeal = getDealById(id)),
    ($$("#modal-deal").style.display = "block"),
    ($$("#deal-popup-line_1").innerHTML = selectedDeal.deal.line_1),
    selectedDeal.deal.line_2 &&
        ($$("#deal-popup-line_2").innerHTML = selectedDeal.deal.line_2),
        ($$("#deal-popup-code").value = selectedDeal.deal_code),
        ($$("#deal-popup-domain").innerHTML = store.domain);
    const flagEl = $$("#deal-popup-flag"),
        noflagEl = $$("#deal-popup-noflag");
    selectedDeal.flag ?
        ((flagEl.innerHTML = selectedDeal.flag.label),
            flagEl.classList.add(
                `coupon-heading-flag-${selectedDeal.flag.color}`
            ),
            (flagEl.style.display = "inline-block"),
            (noflagEl.style.display = "none")) :
        ((noflagEl.style.display = "inline-block"),
            (flagEl.style.display = "none")),
        openDealPopup(),
        dataLayer.push({ event: "showDeal", dealCode: selectedDeal.deal_code }),
        store.monetised_link &&
        !didMonetisedExit &&
        (dataLayer.push({
                event: "monetisedExit",
                externalClickURL: store.store.link
            }),
            (window.pagesense = window.pagesense || []),
            window.pagesense.push(["trackEvent", "Monetized Out"]),
            (didMonetisedExit = !0)),
        dealAction(store.slug, selectedDeal.deal_id, "used", 1);
}

function openDealPopup() {
    ($$(".popup").style.display = "flex"),
    ($$("#deal-popup-button").innerHTML = "COPY"),
    resetQA();
}

function modalBackgroundClick() {
    chromeAdDisplayed ? closeChromeAd() : closeDealPopup();
}

function closeDealPopup() {
    ($$(".popup").style.display = "none"),
    ($$("#modal-deal").style.display = "none"),
    dataLayer.push({ event: "hideDeal", dealCode: selectedDeal.deal_code });
}

function copyCode() {
    var input = $$("#deal-popup-code");
    if (is_ios) {
        (input.contenteditable = !0), (input.readonly = !1);
        var range = document.createRange();
        range.selectNodeContents(input);
        var s = window.getSelection();
        s.removeAllRanges(),
            s.addRange(range),
            input.setSelectionRange(0, 999999);
    } else input.select();
    document.execCommand("copy"),
        ($$("#deal-popup-button").innerHTML = "COPIED"),
        $$("#deal-popup-button").focus(),
        dataLayer.push({ event: "copyDealCode", dealCode: input.value });
}

function nextCode() {
    dealAction(store.slug, selectedDeal.deal_id, "working", "no"),
        dataLayer.push({ event: "qaAnswer", qid: "code_working", value: "no" });
    const nextDealIndex = selectedDeal.index + 1;
    selectDeal(store.deals[nextDealIndex].deal_id);
}

function resetQA() {
    qaIndex = 0;
    const questionsEl = $$(".modal-qa-container"),
        questionEl = $$(".qa-modal-question-yn"),
        thanksEl = $$(".modal-qa-question-thanks"),
        addedQuestions = $$$(".qa-modal-question-added");
    Array.from(addedQuestions).forEach(q => questionsEl.removeChild(q));
    const q0 = $$("#q0"),
        q1 = $$("#q1");
    thanksEl.classList.remove("qa-position-focus"),
        thanksEl.classList.add("qa-position-right"),
        q0.classList.remove("qa-position-left"),
        q0.classList.add("qa-position-focus"),
        q1.classList.remove("qa-position-left"),
        q1.classList.remove("qa-position-focus"),
        q1.classList.add("qa-position-right"),
        qaQuestions.forEach(q => {
            const newQuestionEl = questionEl.cloneNode(!0);
            newQuestionEl.classList.remove("qa-position-focus"),
                newQuestionEl.classList.add("qa-position-right"),
                newQuestionEl.classList.add("qa-modal-question-added"),
                (newQuestionEl.id = `q${q.index}`),
                (newQuestionEl.querySelector(
                    ".qa-question-question"
                ).innerHTML = q.question);
            const buttons = newQuestionEl.querySelectorAll(".qa-answer-button");
            (buttons[0].onclick = () => qaAnswer(q.qid, "yes")),
            (buttons[1].onclick = () => qaAnswer(q.qid, "no")),
            (buttons[2].onclick = () => qaAnswer(q.qid, "not sure")),
            questionsEl.insertBefore(newQuestionEl, thanksEl);
        }),
        (thanksEl.id = `q${qaQuestions.length + 2}`),
        selectedDeal.index === store.deals.length - 1 &&
        $$(".qa-position-focus").classList.remove("qa-has-next-deal");
}

function qaAnswer(qid, value) {
    value ||
        "save_value" !== qid ||
        (value = $$("#qa-save-value").value.replace("$", ""));
    let url = `/submitqa?slug=${store.slug}&qid=${qid}&value=${value}&deal_id=${selectedDeal.deal_id}`;
    qid && value && fetch(url).then(res => res.json());
    let action_event = null,
        action_value = null;
    "save_value" === qid &&
        ((action_event = "save_value"), (action_value = value)),
        "code_working" !== qid ||
        ("yes" !== value && "no" !== value) ||
        ((action_event = "working"), (action_value = "yes" === value)),
        action_event &&
        action_value &&
        dealAction(
            store.slug,
            selectedDeal.deal_id,
            action_event,
            action_value
        ),
        "code_working" === qid && "no" === value && nextQA(),
        nextQA(),
        dataLayer.push({ event: "qaAnswer", qid: qid, value: value });
}

function nextQA() {
    $$(`#q${qaIndex}`).classList.add("qa-position-left"),
        $$(`#q${qaIndex}`).classList.remove("qa-position-focus"),
        $$(`#q${qaIndex + 1}`).classList.add("qa-position-focus"),
        $$(`#q${qaIndex + 1}`).classList.remove("qa-position-right"),
        qaIndex++;
}

function $$(selector) {
    return document.querySelector(selector);
}

function $$$(selector) {
    return document.querySelectorAll(selector);
}

function getDealById(id) {
    return store.deals.filter(d => d.deal_id === id)[0];
}

function storeLinkClick() {
    dataLayer.push({
        event: "externalLinkClick",
        externalClickURL: store.domain
    });
}

function dealAction(slug, deal_id, event, value) {
    let tags = "";
    store.store.tags.forEach(tag => (tags += `&tags=${tag}`)),
        fetch(
            `/deal/action?slug=${slug}&deal_id=${deal_id}&event=${event}&value=${value}${tags}`
        );
}

function showSearch() {
    $$(".top-nav-items").classList.toggle("top-nav-items-search-show"),
        $$("#search-img-open").classList.toggle("top-nav-items-search-show"),
        $$("#search-img-close").classList.toggle("top-nav-items-search-show"),
        $$(".nav-search").classList.toggle("search-box-hide"),
        $$("#search-input").focus();
}
chromeModalEl &&
    chromeModalEl.addEventListener("click", e => e.stopPropagation()),
    $$("#modal-deal").addEventListener("click", e => e.stopPropagation()),
    ($$("#qa-save-value").oninput = e => {
        const val = e.target.value.replace(/[^\d\.]/g, "");
        e.target.value = `$${val}`;
    });
var client = algoliasearch("JJ05T1BWQJ", "541fc96cc6c2fce16e7188d768389ccd"),
    index = client.initIndex("prod_STORES");

function checkChromeExtensionDisplay() {
    const chromeAd = $$(".ext-row"),
        emailAd = $$(".email-row");
    is_desktop && is_chrome ?
        (chromeAd && chromeAd.classList.add("ext-row-show"),
            (emailAd.hidden = !0),
            dataLayer.push({ event: "chromeExtensionAd", action: "displayed" })) :
        chromeAd && chromeAd.classList.remove("ext-row-show");
}

function extensionAdClick() {
    dataLayer.push({ event: "chromeExtensionAd", action: "clicked" });
}
autocomplete("#search-input", { hint: !1 }, [{
        source: autocomplete.sources.hits(index, { hitsPerPage: 5 }),
        displayKey: "name",
        templates: {
            suggestion: function(suggestion) {
                return (
                    '<img src="https://img.wethrift.com/' +
                    suggestion.objectID +
                    '.jpg" onerror="this.src=\'https://img.wethrift.com/error.jpg\'"/><span>' +
                    suggestion._highlightResult.name.value +
                    "</span>"
                );
            }
        }
    }]).on("autocomplete:selected", function(event, suggestion, dataset) {
        dataLayer.push({ event: "searchClick", searchResult: suggestion.objectID }),
            (window.location.href = "/" + suggestion.objectID);
    }),
    checkChromeExtensionDisplay();
let emailIsValid = !0;
const emailInput = $$("#email"),
    emailButton = $$(".email-button"),
    faqEmailInput = $$(".faq-subscribe input");

function emailSubmit() {
    const email = emailInput.value;
    validateEmail(email) ?
        (sendEmail(email),
            (emailInput.hidden = !0),
            (emailButton.hidden = !0),
            $$(".email-confirm").classList.add("email-confirm-show"),
            dataLayer.push({ event: "emailCapture", action: "submission" })) :
        alert("Please enter a valid email address.");
}

function faqEmailSubmit() {
    const email = faqEmailInput.value;
    console.log(email),
        validateEmail(email) ?
        (sendEmail(email),
            dataLayer.push({ event: "emailCapture", action: "submission" }),
            ($$(".faq-subscribe input").hidden = !0),
            ($$(".faq-subscribe button").hidden = !0),
            ($$(".faq-subscribe-success").style.display = "inline-block")) :
        alert("Please enter a valid email address.");
}

function sendEmail(email) {
    const tagsParams = store.store.tags.map(tag => `&tags=${tag}`).join(""),
        url = `/email-subscribe?email=${email.toLowerCase()}&store_slug=${
            store.slug
        }${tagsParams}`;
    fetch(url);
}

function emailBlur(e) {
    (emailIsValid = validateEmail(e.target.value)), setEmailStyles();
}

function emailFocus(e) {
    (emailIsValid = !0), setEmailStyles();
}

function setEmailStyles() {
    emailIsValid
        ?
        ((emailInput.style["border-color"] = "black"),
            (emailButton.style["border-color"] = "black")) :
        ((emailInput.style["border-color"] = "red"),
            (emailButton.style["border-color"] = "red"));
}

function validateEmail(email) {
    return !email || /[^\s@]+@[^\s@]+\.[^\s@]+/.test(email);
}
emailInput && emailInput.addEventListener("blur", emailBlur),
    emailInput && emailInput.addEventListener("focus", emailFocus);