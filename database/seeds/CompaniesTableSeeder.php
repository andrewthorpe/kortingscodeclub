<?php

use Illuminate\Database\Seeder;
use App\Company;
use App\Coupon;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Company::class, 25)->create()->each(function ($company) {
            $company->coupons()->saveMany(factory(Coupon::class, rand(3, 20))->make());
        });
    }
}
