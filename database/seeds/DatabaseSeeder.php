<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // this seeeder also seeds relational Coupons!
        $this->call(CompaniesTableSeeder::class);
    }
}