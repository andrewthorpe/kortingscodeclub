<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');
                $table->string('name');
                $table->string('url');
                $table->text('about');
                $table->boolean('top_choice')->default(false); // make a company a top choice (listen in top companies)

                $table->boolean('free_returns')->nullable();
                $table->boolean('international_shipping')->nullable();
                $table->boolean('free_shipping')->nullable();
                $table->decimal('average_saving', 10, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
