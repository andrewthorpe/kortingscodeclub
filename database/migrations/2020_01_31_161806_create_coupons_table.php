<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->bigIncrements('id');
                $table->bigInteger('company_id')->unsigned()->index();
                $table->foreign('company_id')->references('id')->on('companies');
                $table->string('title');
                $table->string('code');
                $table->boolean('top_choice')->default(false); // make a coupon a top choice (listen on top in view)
                $table->integer('status')->default(10); // verified, new, ...? (new = 1)
                $table->integer('used')->default(0);
                $table->date('last_used')->nullable();
                $table->integer('success_rate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
