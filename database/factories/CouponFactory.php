<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Coupon;

$factory->define(Coupon::class, function (Faker $faker) {

    $titles = ['30% OFF', '15% DISCOUNT', '20% OFF AT CHECKOUT', '20% OFF'];
    $status = [10, 20];

    return [
        'title' => $titles[array_rand($titles)],
        'code' => $faker->swiftBicNumber,
        'top_choice' => $faker->boolean($chanceOfGettingTrue = 15),
        'status' => $status[array_rand($status)],
        'used' => $faker->numberBetween($min = 1, $max = 300),
        'last_used' => $faker->dateTime($max = 'now', $timezone = null),
        'success_rate' => $faker->numberBetween($min = 1, $max = 99),
    ];
});