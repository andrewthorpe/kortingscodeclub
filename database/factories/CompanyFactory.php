<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Company;

$factory->define(Company::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'url' => $faker->url,
        'about' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
        'top_choice' => $faker->boolean($chanceOfGettingTrue = 15),
        'free_returns' => $faker->boolean($chanceOfGettingTrue = 50),
        'international_shipping' => $faker->boolean($chanceOfGettingTrue = 50),
        'free_shipping' => $faker->boolean($chanceOfGettingTrue = 50),
        'average_saving' => $faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 99)
    ];
});